<?php
/**
 * @file
 * Form default button installation hooks.
 */

/**
 * Implements hook_schema().
 */
function form_default_button_schema() {
  $schema['form_default_buttons'] = array(
    'description' => 'This table describes stores the default button settings for individual forms.',
    'fields' => array(
      'form_id' => array(
        'description' => 'The form_id of the form on which to override default button.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'default_action' => array(
        'description' => 'Basic configuration setting for altering default button behavior.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => 'browser_default',
      ),
      'configuration' => array(
        'description' => 'Serialized additional configuration settings for changing default button behavior.',
        'type' => 'blob',
        'size' => 'normal',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('form_id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function form_default_button_install() {
  $form_ids = array(
    'contact_site_form',
    'contact_personal_form',
    'user_register_form',
    'user_pass',
    'user_login',
    'user_login_block',
  );

  // Add form_ids of all currently known node types too.
  if (function_exists('node_type_get_names')) {
    foreach (node_type_get_names() as $type => $name) {
      $form_ids[] = $type . '_node_form';
      $form_ids[] = 'comment_node_' . $type . '_form';
    }
  }

  foreach ($form_ids as $form_id) {
    db_insert('form_default_buttons')
      ->fields(array(
        'form_id' => $form_id,
        'default_action' => 'browser_default',
        'configuration' => serialize(array()),
      ))
      ->execute();
  }
}
